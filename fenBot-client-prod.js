/*!
fenBot-client.js
auteur:pascal TOLEDO
git:https://gitorious.org/fenbot
*/

/**
 * Retourne la valeur d'un paramètre d'une url
 * 
 * @param string param
 * nom du paramètre dont on souhaite avoir la valeur
 * @param url
 * url dans laquel on souhaite récupérer le paramètre ou rien si l'on souhaite travailler sur l'url courante
 * @return String
 * @author Labsmedia
 * @see http://www.labsmedia.com
 * @licence GPL
 * exemple: http://dev.petitchevalroux.net/javascript/recuperer-parametre-dans-une-url-javascript.140.html
 */
function getParamValue(param,url)
{
        var u = url == undefined ? document.location.href : url;
        var reg = new RegExp('(\\?|&|^)'+param+'=(.*?)(&|$)');
        matches = u.match(reg);
        return (matches && matches[2]) ? decodeURIComponent(matches[2]).replace(/\+/g,' ') : '';
}

function consoleAdd(txt){document.getElementById('console').innerHTML+=txt+'<br>';}
if((typeof(console)==='object') && !console.log) console.log=consoleAdd;
if(!console)console=function(){this.log=consoleAdd;}



var reseau=getParamValue('reseau');
//document.write('reseau:'+reseau);
var serveurNS=(reseau=='graspNet')?'192.168.100.253':'mediatheques.legral.fr';


//var serveurNS='127.0.0.1'; // attention avec la same origin policy teste ic-dijon.fr vers mail.ic-dijon.fr
//var serveurNS='mediatheques.legral.fr';
var serveurPORT=10668;
//var salon="ic-dijon";
var salon="coagul";

var frameIdCSS=null
var fd=null;	// frameDef instance

function frameDef(param){
	p=(typeof(param)=="object")?param:{};
        this.salon=(p.salon)?p.salon:'legral';

	this.timerId=null;
        this.setTimer=p.setTimer?p.setTimer:0;	// activation du timer?
//        this.delayTimer=!isNaN(p.delayTimer)?p.delayTimer:5000;

	intervalIdCSS=document.getElementById('interval');
//	var i=intervalIdCSS.value;
//	this.i*1000;

	this.frameIdCSS=document.getElementById('frameBuffer');
	this.botAdresseIdCSS=document.getElementById('botAdresse');
	this.botAdresseIdCSS.innerHTML='adresse du bot:'+serveurNS+' interval: '+interval+'s<br>';

	this.frameRefresh=function(param){
		p=(typeof(param)=="object")?param:{};
		if(p.setTimer)this.setTimer=p.setTimer;
		
		this.frameUrl='http://'+serveurNS+':'+serveurPORT+"?getData=buffer&salon="+this.salon;
	        this.frameIdCSS.src=this.frameUrl;
        
	        if(this.setTimer){
			if(this.timerId)clearTimeout(this.timerId);
			var i=intervalIdCSS.value;
			this.delayTimer=i*1000;
			if(this.delayTimer)
				this.timerId=setInterval("fd.frameRefresh()",this.delayTimer)
			else	this.botAdresseIdCSS.innerHTML='Rafraichissement STOPPER. Vous devez relancr la page pour le r&eacute;activer!';
			this.botAdresseIdCSS.innerHTML='adresse du bot:'+serveurNS+' interval: '+i+'s<br>';
				
//			this.setTimer=0;
			}
		this.setTimer=1; // on initialise le timer a chaque fois
		}
	}


//fenBot_main()
	fd=new frameDef({"salon":salon,"delayTimer":5000});
        fd.frameRefresh({"setTimer":1});


