#!/usr/bin/env node
/*!
fenBot
auteur: pascal TOLEDO
date creation: 2014.04.13
date modification: 2014.07.06
licence: GPLv3
*/

var FICHIER_NOM="fenBot-irc.js";
var FENBOT_VER='0.2.7';

console.log('******************************************************');
t='demarrage de '+FICHIER_NOM+' ver:'+FENBOT_VER;
console.log(t);
console.log('******************************************************');


// -- Chargement de le coloration de la console -- //
var colors = require('/usr/local/lib/node_modules/colors');
colors.setTheme({silly: 'rainbow',input: 'yellow',verbose: 'cyan',prompt:  'grey',info: 'green',data: 'blue',help: 'cyan',warn: 'yellow',debug: 'grey',error: 'red'});


/**********************************************
  ****  VARIABLES GLOBALES   **** 
***********************************************/
var DEV=(process.argv[2]=='dev=1')?1:0;	// Attention coder en dur // node fenBot-irc.js dev=1
if(DEV===1){t="mode de develloppement";console.log(t.info);}
else{t="mode de production";console.log(t.info);}

var debug='err';	// changer via l'irc
debug=0;
var httpSrvr=null;
var HTTPPORT=10668,HTTPHOST='';
var bot=null;

var ircDN='barjavel.freenode.net';
if(DEV)ircDN='127.0.0.1';


t="Contient le bot irc et le serveur http\n";
t+="le bot irc recoit les donnee de l'irc \n";
t+="les met dans un buffer circulaires\n";
t+="les donnees sont envoyes au client web via le http.\n";
console.log(t.info);


t="chargement des libs tiers";console.log(t.info);


/**********************************************
  ****  CHARGERMENT/CREATION/CONNEXION   **** 
***********************************************/


/**********************************************
 gestion des buffer 
***********************************************/
// - version de production - //
//lib='/www/sites/intersites/lib/perso/js/gestBufcirc/gestBufcirc-0.2/gestBufcirc.js';    // prod
//lib='/www/sites/intersites/lib/perso/js/gestBufcirc/versions/gestBufcirc-v0.3.1.js';    // prod
lib='/www/sites/intersites/lib/perso/js/gestBufcirc/versions/gestBufcirc-v0.3.3.js';

// - version de develloppement - //
if(DEV==1)lib='/www/git/intersites/lib/perso/js/gestBufcirc/gestBufcirc.js';            // dev

//console.log(lib.data);

t="chargement de libgestBufCirc ("+lib+")...";console.log(t.debug);
var libgestBufCirc=require(lib);

var bufCirc=[];
bufCirc['ic-dijon']= new libgestBufCirc.gestBufCirc({"nom":"ic-dijon","autoload":1,pushBeforeSave:5,MAX:20});
bufCirc['legral']  = new libgestBufCirc.gestBufCirc({"nom":"legral",  "autoload":1,pushBeforeSave:5,MAX:10});
bufCirc['coagul']  = new libgestBufCirc.gestBufCirc({"nom":"coagul",  "autoload":1,pushBeforeSave:5,MAX:40});

t="etat des buffers:";console.log(t.data);
console.dir(bufCirc);

var exec=require('child_process').exec;

/**********************************************
  ****  CHARGERMENT/CREATION/CONNEXION   **** 
***********************************************/


/**********************************************
  ==  httpSrv : creation du serveur ==
serveur http en liaison avec le client web
le lcient sera mise a jours via ajax
***********************************************/
//  - creation du serveur http - //
t="creation du server http ("+HTTPHOST+":"+HTTPPORT+"): reussi";console.log(t.debug);
var httpSrv = require('http').createServer(httpSrvImplement);
httpSrv.listen(HTTPPORT,HTTPHOST);


/**********************************************
  ==  ircBot : creation du bot ==
Se connecte au serveur irc et interagie avec lui

https://node-irc.readthedocs.org/en/latest/API.html#internal

{ prefix: 'fenwe!~pascal@ip-167.net-89-2-21.rev.numericable.fr',
  nick: 'fenwe',
  user: '~pascal',
  host: 'ip-167.net-89-2-21.rev.numericable.fr',
  command: 'PRIVMSG',
  rawCommand: 'PRIVMSG',
  commandType: 'normal',
  args: [ '#ic-dijon', 'qqq' ] }

***********************************************/
ll= "/usr/local/lib/node_modules/irc";
console.log('--- chargement de '+ll+' ---'.debug);
var irc = require(ll);
var ircClientNb=0;
// ---------------------------------------------------------------- //
console.log("demarrage du bot de Fenwe: fenBot".info);
var fenBotConfig={
server: ircDN

,options:{
    userName: 'fenBot',
    realName: 'fenwe Bot v'+FENBOT_VER,
    port: 6667,
    debug: false,
    showErrors: true,
    autoRejoin: true,
    autoConnect: true,
    channels:  ["#fenBot","#legral","#coagul","#ic-dijon"],
    secure: false,
    selfSigned: false,
    certExpired: false,
    floodProtection: true,
    floodProtectionDelay: 1000,
    sasl: false,
    stripColors: false,
    channelPrefixes: "&#",
    messageSplit: 512
    }
}


// --- irc: creation du botnet --- //
t="creation du bot...";console.log(t.debug);
var bot = new irc.Client(fenBotConfig.server, fenBotConfig.options.userName, fenBotConfig.options);

t="En cours de connexion a "+fenBotConfig.server+" (asynchrone).";console.log(t.debug);

/**********************************************
         ****  IMPLEMENTATION   **** 
***********************************************/

/**********************************************
  ==  ircBot : implementation ==
Se connecte au serveur irc et interagie avec lui
***********************************************/

// --- general --- //
bot.addListener('error', function(message) {
	console.log("reception d' une error: ", message.error);
	});

// Listen for joins
bot.addListener("join", function(channel, who) {
	// Welcome them in!
	if(who!=fenBotConfig.options.userName){
		if((channel=='#legral')||(channel=='#ic-dijon')){var t= who.data + " bienvenue sur le chan "+channel.verbose; bot.say(channel,t);}
		}
	});

//-------------------------------//
// -- ecouter les evenemments -- //
//-------------------------------//

// --- raw --- //
/*
bot.addListener("raw", function(message) {
	switch(message.rawCommand)
		{
		case '255': var t="fonction raw 255 :"+message.rawCommand;console.log(t.info);console.dir(message);  break;
                case '256': var t="fonction raw 256 :"+message.rawCommand;console.log(t.info);console.dir(message);  break;
                case 'PING':  break;
                case '376': var t="fonction raw 376 :"+message.rawCommand;console.log(t.info);console.dir(message);  break;

//		default:console.log("fonction raw: default: ".info);console.dir(message);
		
		}
	});
*/

/*
retourne une ligne texte enrichie de code html correspondant a un template design
arg:
 - text:
 - nu: numero du template
 - from:pseudo ou texte isole
*/
function lineTemplate(text,nu,from){
	if(!text)return '';
	if(!nu)nu=0;
	
	var dt=new Date();
	var h=dt.getHours();  h=(h<10)?'0'+h:h;
	var m=dt.getMinutes();m=(m<10)?'0'+m:m;
	var time=h+':'+m;

        var couleur='white';
        switch(from){
                case'fenwe':couleur="#0F0";break;
                case'anais':couleur="#D52";break;
                case'vato21':couleur="#11F";break;
                }

	switch(nu){
		case 0:
        		text='<b>'+from+'</b>:'+text;
		        text='<span style="color:'+couleur+'">'+text+'</span>';
		        text='<span style="color:#AAA">['+time+']</span>'+text;
		        text+='<br>';
			return text;
			break;
		}
	return text;
	}

/********************************************
Retourne le stdout formater selon un template
arg:
*options:
 - templateNu:
 - before
 - after
********************************************/
function stdoutTemplate(error,stdout,stderr,options){
	var t='';
	var opt=(typeof(options)=='object')?options:{};
	
	if(!opt.template)opt.template=0;
	if(!opt.before)opt.before='';
	if(!opt.after)opt.after='';
	if(!opt.cmd)opt.cmd='';
	
	var out='';
	switch(opt.template){
                case 0:
		case 'err':
                        out+=opt.before;
			if(opt.cmd!='')out+=opt.cmd+'\n';
                        out+=stdout;
			if(opt.template=='err'){out+='(error:'+error+')'}
			if(opt.template=='err'){out+='(stderr:'+stderr}
                        out+=opt.after;
                        return out;
                        break;
                }
	return stdout;

	}


// ---- reception d'un message privé (a destination du bot) ---- //
bot.addListener('pm', function (from, message) {
	console.log("fonction pm");
	console.log("message de"  + from + ":"+ message);
//	bot.say(fenBotConfig.options.channels[0], irc.colors.wrap("light_green","message prive ecrit dans ?"));
//	envoyer un pm a l'emetteur pour confirmation?
	});



// --- ******* --- //
// --- #fenBot --- //

// ---- reception d'un message dans le canal #fenbot ---- //
bot.addListener("message#fenbot", function(from, to, text, message) {
        console.log("fonction message#fenbot");
	var child=null;
	var cmd=text.args[1];

	var sshnas='ssh -p 10122 user@192.168.100.251 ';
	if(from=='fenwe'){
		switch(cmd){
			
			// -- fenBot -- //
			case"!node:uptime":bot.say("#fenBot","uptime:"+process.uptime());break;
			case"!version":bot.say("#fenBot","fenBot version :"+FENBOT_VER);break;
			case"!help":
				out=' === fenBot v'+FENBOT_VER+': ===\n';
				out+="!help fenBot\n";
				out+="!help rpi1\n";
				out+="!help nas\n";
				bot.say("#fenBot",out);
				break;

			case"!help fenBot":
				out='';
				out+="!node uptime\n";
				out+="!version\n";
				out+="!fenBot:halt\n";
				out+="!log:saves\n";
				out+="!log:clear\n";
				out+="!log:show salon\n";
				out+="!debug\n";
				bot.say("#fenBot",out);
				break;

			 case"!help rpi1":
				out=' === rpi1: ===\n';
				out+="!uptime\n";
				out+="!df\n";
				out+="!playSound\n";
				bot.say("#fenBot",out);
				break;

			 case"!help nas":
				out=' === nas: ===\n';
				out+="!nas:start\n";
				out+="!nas:stop\n";
				out+="!nas:reboot\n";
				out+="!nas:uptime\n";
				out+="!nas:sensors\n";
				out+="!nas:hddtemp\n";
				out+="!nas:hddtemp sda\n";
				out+="!nas:df\n";
				out+="!nas:lsblk\n";
				out+="!nas:mount|umount|eject docutheques|install\n";
				out+="!nas:minidlna reload\n";

				bot.say("#fenBot",out);
				break;

			

			// -- fenBot-- //
			case "!fenBot:halt":
				// - demande de quitter le programme - //
				bot.say("#fenBot",'arretr demandé...');
				// -- on sauve les buffer -- //
				bot.say("#fenBot",'sauvegarde des buffers');
				bufCirc['ic-dijon'].save();
				bufCirc['legral'].save();
				bufCirc['coagul'].save();

				// -- on sauve les buffer -- //
                                bot.say("#fenBot",'deconnexion (non implementé');
				process.exit(1);	// quitte le programme
				break;

			case"!log:clear":
                                bufCirc['ic-dijon'].clear();
                                bufCirc['legral'].clear();
                                bufCirc['coagul'].clear();
                                break;

			case"!log:show ic-dijon":bot.say("#fenBot", bufCirc['ic-dijon'].show({"beginToZero":1}));break;
			case"!log:show coagul":  bot.say("#fenBot", bufCirc['coagul'].show  ({"beginToZero":1}));break;
			case"!log:show legral":  bot.say("#fenBot", bufCirc['legral'].show  ({"beginToZero":1}));break;

			case'!debug':bot.say("#fenBot",'debug='+debug);break;
			case'!debug=0':debug=0;bot.say("#fenBot",'maintenant debug='+debug);break;
			case'!debug=err':debug='err';bot.say("#fenBot",'maintenant debug='+debug);break;			


			// -- rpi1 -- //
                        case"!uptime":child=exec('uptime',                                  function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
                        case"!df":child=exec('df -h',                                       function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
                        case"!playSound":child=exec('cvlc --play-and-exit /home/musiques/oscillofun.flac',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;

			// -- nas -- //
			case"!nas:stop":   child=exec(sshnas+'sudo shutdown -hf -t 30 now ',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:reboot": child=exec(sshnas+'sudo shutdown -rf -t 30 now ',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;

			case"!nas:uptime": child=exec(sshnas+'uptime ',                     function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;

			case"!nas:sensors":child=exec(sshnas+'sensors',                     function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:hddtemp":child=exec(sshnas+'"sudo hddtemp /dev/sd*" ',    function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:hddtemp sda":child=exec(sshnas+'"sudo hddtemp /dev/sda" ',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:hddtemp sdc":child=exec(sshnas+'"sudo hddtemp /dev/sdc" ',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:hddtemp sdd":child=exec(sshnas+'"sudo hddtemp /dev/sdd" ',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:hddtemp sde":child=exec(sshnas+'"sudo hddtemp /dev/sde" ',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:hddtemp sdf":child=exec(sshnas+'"sudo hddtemp /dev/sdf" ',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:df":        child=exec(sshnas+'df -h | grep /partitions/',
			function(error,stdout,stderr){
				var et='',t='';
				et="Sys. fich.     Taille Util. Dispo Uti% Monté sur";
//				et=irc.colors.wrap(irc.colors.codes.light_cyan,et);
				bot.say("#fenBot",et);
				t+=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t);
				});
				break;

			case"!nas:lsblk": child=exec(sshnas+'"sudo lsblk"',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:mount docutheques": child=exec(sshnas+'"sudo  mount /partitions/docutheques"',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:umount docutheques":child=exec(sshnas+'"sudo umount /partitions/docutheques"',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
			case"!nas:eject docutheques": child=exec(sshnas+'"sudo eject  /partitions/docutheques"',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;

                        case"!nas:mount install": child=exec(sshnas+'"sudo  mount /partitions/install"',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
                        case"!nas:umount install":child=exec(sshnas+'"sudo umount /partitions/install"',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;
                        case"!nas:eject install": child=exec(sshnas+'"sudo eject  /partitions/install"',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;

			case"!nas:minidlna reload": child=exec(sshnas+'"sudo /etc/init.d/minidlna force-reload"',function(error,stdout,stderr){t=stdoutTemplate(error,stdout,stderr,{"template":debug});bot.say("#fenBot",t)});break;

			// -- provoque un flood -- //
//                        case"!netstat":child=exec('netstat --tcp | tail',function(error,stdout,stderr){bot.say("#fenBot",stdout);});break;

//                        case"!ps":child=exec('ps -A',function(error,stdout,stderr){bot.say("#fenBot",stdout);});break;

			}

		}

	// ---- appelles valides pour tout le monde ---- //
	switch(cmd){
		case"!nas:start":child=exec('wakeonlan 00:30:05:fd:83:df',function(error,stdout,stderr){bot.say("#fenBot",stdout);});break;

		case"!log:saves":
			bot.say("#fenBot",cmd);
                        bufCirc['ic-dijon'].save();
                        bufCirc['legral'].save();
                        bufCirc['coagul'].save();
                        break;
		}

	});


// --- ******* --- //
// --- #legral --- //
bot.addListener("message#legral", function(from, to, text, message) {
	var textOn=text.args[1]; //console.log(textOn.data);

        textOn=lineTemplate(textOn,0,from);
        bufCirc['legral'].push(textOn);

	});

// --- ******* --- //
// --- #coagul --- //
bot.addListener("message#coagul", function(from, to, text, message) {
        var textOn=text.args[1]; //console.log(textOn.data);
        textOn=lineTemplate(textOn,0,from);
        bufCirc['coagul'].push(textOn);
	});


// --- ********* --- //
// --- #ic-dijon --- //
bot.on("names#ic-dijon",function(s)
        {
        console.log("names#ic-dijon".info);
        console.dir(s);
        var nb=0;
        for (var i in s){nb++;}
        //nb--; // on enleve fenbot dans le total
        var t="nb d'user:"+nb ;console.log(t.data);
        });

bot.addListener("message#ic-dijon", function(from, to, text, message) {
	var textOn=text.args[1]; //console.log(textOn.data);
	textOn=lineTemplate(textOn,0,from);
	bufCirc['ic-dijon'].push(textOn);
	});


// -- faire des actions -- //
// --- demander le nombre d'user dans un chan--- //
bot.send("255");



/**********************************************
  ==  fenBot : fonction  ==
fonction pour le fenBot
retourne 0 si valider; 1 si non valider
***********************************************/
function list(pseudo,text,opt){
	if(typeof(otp)!=='object')opt={};
	if(text=="!list"){
		if((opt.all===1)||pseudo=='fenwe'||pseudo=='vato21' )
			{
			bot.say("#fenBot","fenBot list des command :");
			return 0;
			}
		}
	return 1;
	}
function list(pseudo,text,opt){
        if(typeof(otp)!=='object')opt={};
        if(text=="!list"){
                if((opt.all===1)||pseudo=='fenwe'||pseudo=='vato21' )
                        {
                        bot.say("#fenBot","fenBot list des command :");
                        return 0;
                        }
                }
        return 1;
        }



/**********************************************
  ==  httpSrv : implementation  ==
serveur http en liaison avec le client web
 - envoie les donnees (le client les recoit via Ajax)
***********************************************/
function httpSrvImplement(req, res){
	var t="httpSrvImplement";//console.log(t.debug);
        var psp=require('url').parse(req.url, true);

        var fileType='text/html'
	,content="Aucune demande faite";

//	console.dir(psp.query['salon']);
	var salon=psp.query['salon']?psp.query['salon']:"ic-dijon";
//	content="salon:"+salon;
        console.dir(salon);
	console.dir(psp);
        switch(psp.query['getData']){
                //case 'index':content="";break;
                //case 'useNb':content=;break;

		case 'buffer':

		        if(!bufCirc[salon]){
				t="";console.log(t.debug)
		                t="pas de salon '"+salon+"'enregistre (ne pas mettre le #)";
		                console.log(t.error);
		                content=t;
		                }
			else{
				content=bufCirc[salon].show({"beginToZero":1});
//				t="le buffer du salon '"+salon+"' existe on le copy, content: ";console.log(t.error);
//				console.dir(content);				
				if(content==""){
					t="aucun message dans le salon: "+salon;
//					console.log(t.error);
	                                content=t;
					}
				}
			break;

               }
	var head="<html><style>body{background-color:#000;color:#FFF}</style>";
	content=head+content+"</html>";
//	t="content:"+content;console.log(t.data);
        res.writeHead(200,{"Content-Type": fileType});
        res.write(content);
        res.end();
//        t='les donnees ont ete envoyees';console.log(t.info)
//	t=content;console.log(t.info);
	}; // -- function httpSrvImplement

