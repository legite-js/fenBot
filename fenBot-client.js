/*!
projet: fenBot
fichier: fenBot-client.js
date de creation: 2014.04.18
description: un simple iframe rafraichit avec setinterval pour visualiser les buffers
*/
/* -- configuration -- */
var botIrcDN='127.0.0.1';
//var botIrcDN='192.168.0.253';
//var botIrcDN='mediatheques.legral.fr'; // Nom de Domaine du bot irc
var serveurPORT=10668;
var salon="ic-dijon";
var frameIdCSS=null
var fd=null;	// frameDef instance

/* -- implementation-- */
function frameDef(param){
	p=(typeof(param)=="object")?param:{};
        this.salon=(p.salon)?p.salon:'legral';

	this.timerId=null;
        this.setTimer=p.setTimer?p.setTimer:0;	// activation du timer?
        this.delayTimer=!isNaN(p.delayTimer)?p.delayTimer:5000;

	this.frameIdCSS=document.getElementById('frameBuffer');

	this.frameRefresh=function(param){
		p=(typeof(param)=="object")?param:{};
		if(p.setTimer)this.setTimer=p.setTimer;
		

		this.frameUrl='http://'+botIrcDN+':'+serveurPORT+"?getData=buffer&salon="+this.salon;
	        this.frameIdCSS.src=this.frameUrl;
        
	        if(this.setTimer){
			if(this.timerId)
				clearTimeout(this.timerId);
			this.timerId=setInterval("fd.frameRefresh()",this.delayTimer);
			this.setTimer=0;
			}
		}
	}


function fenBot_main()
        {
	fd=new frameDef({"salon":"ic-dijon","delayTimer":4000});
        fd.frameRefresh({"setTimer":1});
        }

