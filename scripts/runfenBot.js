#!/bin/sh
LOGDIR="/www/log/fenBot";
PROJETDIR="/www/projets/fenBot";
FENBOTLOG="${LOGDIR}/fenBot.log";

echo "*************************************************************************"
echo " demmarage de  $PROJETDIR/fenBot-irc.js"
if [ ! -d $LOGDIR ]; then
  echo " repertoire de log: ${LOGDIR} n'existe pas -> creation";
  mkdir -p $LOGDIR
fi
echo "demarrer le: "  `date` >>  $FENBOTLOG
nohup node $PROJETDIR/fenBot-irc.js >> $FENBOTLOG  &


